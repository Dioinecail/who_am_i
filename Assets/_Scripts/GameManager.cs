﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public TimeController timeController;

    private void Awake()
    {
        Instance = this;
    }

    public void SlowTime(float amount, float duration)
    {
        timeController.SlowDownTime(amount, duration);
    }
}