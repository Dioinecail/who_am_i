﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour, IDestructable
{
    private Rigidbody rb;
    private Animator anim;

    private float _health;
    public float maxHealth;

    public float health
    {
        get
        {
            return _health;
        }

        set
        {
            if (value >= maxHealth)
                _health = maxHealth;
            else if (value <= 0)
            {
                _health = 0;
                Die();
            }
            else
                _health = value;
        }
    }

    public float moveSpeed = 10f;
    public float rotateSpeed = 3;
    public float acceleration = 10;

    public State aiState;

    private int currentWaypoint;
    public float waypointDistanceTreshold;
    public float patrolWaitTime = 1f;
    private float waitTimer;

    public float visibleRange = 5f;
    public float attackRange = 1f;

    public LayerMask visibleMask;

    private Transform visibleTarget;

    public Transform[] waypoints;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    void Update ()
    {
        switch (aiState)
        {
            case State.Patrol:
                Patrol();
                break;
            case State.Chase:
                Chase();
                break;
            case State.Attack:
                Attack();
                break;
            default:
                break;
        }
    }

    #region Patroling

    private void Patrol()
    {
        float distanceToCurrentWaypoint = Vector3.Distance(transform.position, waypoints[currentWaypoint].position);

        bool isInsideDistance = (distanceToCurrentWaypoint < waypointDistanceTreshold);

        if(isInsideDistance)
        {
            waitTimer += Time.deltaTime;

            if(waitTimer >= patrolWaitTime)
            {
                waitTimer = 0;
                currentWaypoint++;
            }
        }

        if (currentWaypoint >= waypoints.Length)
            currentWaypoint = 0;

        if(!isInsideDistance)
        {
            MoveTo(waypoints[currentWaypoint]);
            Rotate();
        }
        else
        {
            Stop();
        }

        Collider[] targets = Physics.OverlapSphere(transform.position, visibleRange, visibleMask);

        if (targets.Length > 0)
        {
            visibleTarget = targets[0].transform;
            aiState = State.Chase;
        }
    }

    #endregion

    #region Chasing


    private void Chase()
    {
        if (visibleTarget == null)
            aiState = State.Patrol;

        if (Vector3.Distance(transform.position, visibleTarget.position) < visibleRange)
        {
            MoveTo(visibleTarget);
            Rotate();
        }
        else
            aiState = State.Patrol;

        if(Vector3.Distance(transform.position, visibleTarget.position) < attackRange)
        {
            aiState = State.Attack;
        }
    }


    #endregion

    #region Attacking


    private void Attack()
    {
        if (visibleTarget == null || Vector3.Distance(transform.position, visibleTarget.position) > attackRange)
            aiState = State.Patrol;

        Stop();
        Rotate();
        anim.Play("Attack", 1);
    }


    #endregion

    #region Moving

    private void MoveTo(Transform target)
    {
        Vector3 moveDirection = target.position - transform.position;
        moveDirection.Normalize();

        moveDirection *= moveSpeed;
        moveDirection.y = rb.velocity.y;

        rb.velocity = Vector3.Lerp(rb.velocity, moveDirection, acceleration * Time.deltaTime);

        anim.SetFloat("Velocity", rb.velocity.magnitude);
    }

    private void Rotate()
    {
        Vector3 lookVector = rb.velocity;

        if (rb.velocity.magnitude < 0.1f)
            return;

        lookVector.y = 0;

        Quaternion targetRotation = Quaternion.LookRotation(lookVector);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotateSpeed * Time.fixedDeltaTime);
    }

    private void Stop()
    {
        Vector3 stopDirection = new Vector3(0, rb.velocity.y, 0);

        rb.velocity = Vector3.Lerp(rb.velocity, stopDirection, acceleration * Time.deltaTime);

        anim.SetFloat("Velocity", rb.velocity.magnitude);
    }

    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, waypointDistanceTreshold);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visibleRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
    }

    public void Die()
    {
        gameObject.SetActive(false);
    }
}

public enum State
{
    Patrol,
    Chase,
    Attack
}
