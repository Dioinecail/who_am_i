﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    public float initialTimeScale = 1f;

    private Coroutine slowDown_cor;

    public void SlowDownTime(float toAmount, float duration)
    {
        if (slowDown_cor != null)
            StopCoroutine(slowDown_cor);

        StartCoroutine(SlowDown(toAmount, duration));
    }

    private IEnumerator SlowDown(float toAmount, float duration)
    {
        float timeElapsed = 0;

        Time.timeScale = toAmount;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;

        while (timeElapsed < duration)
        {
            timeElapsed += Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Lerp(toAmount, initialTimeScale, timeElapsed / duration);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return new WaitForEndOfFrame();
        }

        Time.timeScale = initialTimeScale;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;

        slowDown_cor = null;
    }
}
