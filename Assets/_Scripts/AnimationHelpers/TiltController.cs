﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltController : MonoBehaviour
{
    private Animator tiltAnimator;

    private void Awake()
    {
        tiltAnimator = transform.GetChild(0).GetComponent<Animator>();
    }

    public void ForwardTilt()
    {
        if(!AnimatorIsPlaying())
            tiltAnimator.Play("ForwardTilt");
    }

    public void JumpForwardTilt()
    {
        tiltAnimator.Play("JumpForwardTilt");
    }

    private bool AnimatorIsPlaying()
    {
        return tiltAnimator.GetCurrentAnimatorStateInfo(0).length >
               tiltAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
}