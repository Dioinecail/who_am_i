﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class IKControl : MonoBehaviour
{
    public bool ikActive = false;
    [Space]
    [Header("HANDS")]
    private Vector3 rightHandPosition;
    private Vector3 lastRightHandPosition;
    public Vector3 rightElbowObj;

    private Vector3 leftHandPosition;
    private Vector3 lastLeftHandPosition;
    public Vector3 leftElbowObj;
    [Space]
    [Header("FEET")]
    public Transform rightFeetObj = null;
    public Transform leftFeetObj = null;
    [Space]
    [Header("LOOK")]
    public Transform lookObj = null;

    //public Vector3 leftHandElbow, rightHandElbow;

    [Range(0, 1)]
    public float weight;
    [Range(0, 0.3f)]
    public float weightLerpSpeed;

    private float activeLeftHandWeight;
    private float activeRightHandWeight;
    [Range(0, 1)]
    public float lookWeight;

    public float touchWallRadius;
    public float wallCompensationLength;
    public LayerMask wallLayer;

    public Transform leftShoulder, rightShoulder;
    public Transform rightHandGizmo, lefthandGizmo;

    [Tooltip("Заставляет IK использовать lookWeight для направления лица отдельно от рук")]
    public bool useLookWeight;
    // use to control left and right hand separately
    public bool separateIK;

    private float leftWeight, rightWeight;

    public Animator Anima;

    private void Start()
    {
        Anima.logWarnings = false;
    }

    private void Update()
    {
        SearchForWalls();
    }

    private void SearchForWalls()
    {
        RaycastHit rightWallHit;
        RaycastHit leftWallHit;

        if (Physics.Raycast(rightShoulder.position, transform.right, out rightWallHit, touchWallRadius, wallLayer))
        {
            Vector3 wallCompensation = rightShoulder.position - rightWallHit.point;

            wallCompensation = wallCompensation.normalized * wallCompensationLength;

            rightHandPosition = rightWallHit.point + wallCompensation;
        }
        else
        {
            if (rightHandPosition != Vector3.zero)
                lastRightHandPosition = rightHandPosition;

            rightHandPosition = Vector3.zero;
        }

        if (Physics.Raycast(leftShoulder.position, -transform.right, out leftWallHit, touchWallRadius, wallLayer))
        {
            Vector3 wallCompensation = leftShoulder.position - leftWallHit.point;
            wallCompensation = wallCompensation.normalized * wallCompensationLength;

            leftHandPosition = leftWallHit.point + wallCompensation;
        }
        else
        {
            if (leftHandPosition != Vector3.zero)
                lastLeftHandPosition= leftHandPosition;

            leftHandPosition = Vector3.zero;
        }

        lefthandGizmo.position = leftHandPosition;
        rightHandGizmo.position = rightHandPosition;
    }

    public void TurnLeftIK()
    {
        StartCoroutine(SetIK(AvatarIKGoal.LeftHand, 0, 1, 1));
    }

    public void TurnOffLeftIK()
    {
        StartCoroutine(SetIK(AvatarIKGoal.LeftHand, 1, 0, 1));
    }

    public void TurnRightIK()
    {
        StartCoroutine(SetIK(AvatarIKGoal.RightHand, 0, 1, 1));
    }

    public void TurnOffRightIK()
    {
        StartCoroutine(SetIK(AvatarIKGoal.RightHand, 1, 0, 1));
    }

    //a callback for calculating IK
    private void OnAnimatorIK()
    {
        if (Anima)
        {

            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {

                // Set the look target position, if one has been assigned
                if (lookObj != null)
                {
                    Anima.SetLookAtWeight(useLookWeight ? lookWeight : weight);
                    Anima.SetLookAtPosition(lookObj.position);
                }

                // Set the right hand target position and rotation, if one has been assigned
                if (rightHandPosition != Vector3.zero)
                {
                    activeRightHandWeight = Mathf.MoveTowards(activeRightHandWeight, weight, weightLerpSpeed);

                    Anima.SetIKPositionWeight(AvatarIKGoal.RightHand, separateIK ? rightWeight : activeRightHandWeight);
                    Anima.SetIKRotationWeight(AvatarIKGoal.RightHand, separateIK ? rightWeight : activeRightHandWeight);
                    Anima.SetIKPosition(AvatarIKGoal.RightHand, rightHandPosition);
                    Anima.SetIKRotation(AvatarIKGoal.RightHand, Quaternion.LookRotation(Vector3.up, -transform.right));

                    if (rightElbowObj != Vector3.zero)
                    {
                        Anima.SetIKHintPositionWeight(AvatarIKHint.RightElbow, separateIK ? rightWeight : weight);
                        Anima.SetIKHintPosition(AvatarIKHint.RightElbow, rightElbowObj);
                    }

                    // TODO: Set ik targets for elbows
                    //Anima.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, separateIK ? leftWeight : weight);
                    //Anima.SetIKHintPosition(AvatarIKHint.LeftElbow, transform.position + leftHandElbow);
                }
                else
                {
                    activeRightHandWeight = Mathf.MoveTowards(activeRightHandWeight, 0, weightLerpSpeed);

                    Anima.SetIKPositionWeight(AvatarIKGoal.RightHand, separateIK ? rightWeight : activeRightHandWeight);
                    Anima.SetIKRotationWeight(AvatarIKGoal.RightHand, separateIK ? rightWeight : activeRightHandWeight);
                    Anima.SetIKPosition(AvatarIKGoal.RightHand, lastRightHandPosition);
                    Anima.SetIKRotation(AvatarIKGoal.RightHand, Quaternion.LookRotation(Vector3.up, -transform.right));
                }

                if (leftHandPosition != Vector3.zero)
                {
                    activeLeftHandWeight = Mathf.MoveTowards(activeLeftHandWeight, weight, weightLerpSpeed);

                    Anima.SetIKPositionWeight(AvatarIKGoal.LeftHand, separateIK ? leftWeight : activeLeftHandWeight);
                    Anima.SetIKRotationWeight(AvatarIKGoal.LeftHand, separateIK ? leftWeight : activeLeftHandWeight);
                    Anima.SetIKPosition(AvatarIKGoal.LeftHand, leftHandPosition);
                    Anima.SetIKRotation(AvatarIKGoal.LeftHand, Quaternion.LookRotation(Vector3.up, transform.right));

                    if (leftElbowObj != Vector3.zero)
                    {
                        Anima.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, separateIK ? rightWeight : weight);
                        Anima.SetIKHintPosition(AvatarIKHint.LeftElbow, leftElbowObj);
                    }
                    //Anima.SetIKHintPositionWeight(AvatarIKHint.RightElbow, separateIK ? rightWeight : weight);
                    //Anima.SetIKHintPosition(AvatarIKHint.RightElbow, transform.position + rightHandElbow);
                }
                else
                {
                    activeLeftHandWeight = Mathf.MoveTowards(activeLeftHandWeight, 0, weightLerpSpeed);

                    Anima.SetIKPositionWeight(AvatarIKGoal.LeftHand, separateIK ? leftWeight : activeLeftHandWeight);
                    Anima.SetIKRotationWeight(AvatarIKGoal.LeftHand, separateIK ? leftWeight : activeLeftHandWeight);
                    Anima.SetIKPosition(AvatarIKGoal.LeftHand, lastLeftHandPosition);
                    Anima.SetIKRotation(AvatarIKGoal.LeftHand, Quaternion.LookRotation(Vector3.up, transform.right));
                }

                if (rightFeetObj != null)
                {
                    Anima.SetIKPositionWeight(AvatarIKGoal.RightFoot, weight);
                    Anima.SetIKPosition(AvatarIKGoal.RightFoot, rightFeetObj.position);
                }

                if (leftFeetObj != null)
                {
                    Anima.SetIKPositionWeight(AvatarIKGoal.LeftFoot, weight);
                    Anima.SetIKPosition(AvatarIKGoal.LeftFoot, leftFeetObj.position);
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                Anima.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                Anima.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                Anima.SetLookAtWeight(0);
            }
        }
    }

    private IEnumerator SetIK(AvatarIKGoal ikGoal, float startWeight, float targetWeight, float setTime)
    {

        float elapsedTime = 0;

        while (elapsedTime < setTime)
        {
            elapsedTime += Time.deltaTime * Time.timeScale;

            if (Anima != null)
            {
                switch (ikGoal)
                {
                    case AvatarIKGoal.LeftHand:
                        leftWeight = Mathf.Lerp(startWeight, targetWeight, elapsedTime / setTime);
                        break;
                    case AvatarIKGoal.RightHand:
                        rightWeight = Mathf.Lerp(startWeight, targetWeight, elapsedTime / setTime);
                        break;
                    default:
                        break;
                }
            }
            yield return null;
        }

    }

    private void OnDrawGizmos()
    {
        if (!rightShoulder || !leftShoulder)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, touchWallRadius);
        Gizmos.DrawRay(rightShoulder.position, transform.right * touchWallRadius);
        Gizmos.DrawRay(leftShoulder.position, -transform.right * touchWallRadius);
    }
}