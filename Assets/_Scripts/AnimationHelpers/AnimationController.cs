﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : AbstractBehaviour
{
    public float secondsDelayBeforeLanding;

    public float rootMotionLerpSpeed = 1;

    private Vector3 rootMotion;
    private Vector3 lerpedRootMotion;

    private void OnEnable()
    {
        CollisionBehaviour.onLand += OnLand;    
    }

    private void OnDisable()
    {
        CollisionBehaviour.onLand -= OnLand;
    }

    private void OnLand()
    {
        if (collisionBehaviour.secondsInMidAir < secondsDelayBeforeLanding)
            return;

        Anima.SetTrigger("OnLand");
    }

    private void OnAnimatorMove()
    {
        rootMotion = Anima.deltaPosition;

        rootMotion.y = 0;

        lerpedRootMotion = Vector3.Lerp(lerpedRootMotion, rootMotion, rootMotionLerpSpeed * Time.deltaTime);

        if (Anima.applyRootMotion)
            transform.position += lerpedRootMotion.magnitude * transform.forward;
    }
}