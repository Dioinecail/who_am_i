﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    private Animator anim;
    private MoveBehaviour movement;

    public float slowMoDuration;
    public float slowMoAmount;

    //public float visionRange;
    //public LayerMask targetMask;

    //private List<Transform> targets;
    //private Transform activeTarget;
    //private int activeTargetIndex;

    private void Awake ()
    {
        anim = GetComponent<Animator>();
        movement = GetComponent<MoveBehaviour>();

        //targets = new List<Transform>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            Attack();

        //LookForTargets();

        //if (targets.Count > 0 && Input.GetButtonDown("Tab"))
        //{
        //    SwitchTarget();
        //}
    }

    private void Attack()
    {
        anim.SetTrigger("Attack");
    }

    private void ComboTrue()
    {
        anim.SetBool("Combo", true);
        GameManager.Instance.SlowTime(slowMoAmount, slowMoDuration);
    }

    private void ComboFalse()
    {
        anim.SetBool("Combo", false);
    }

    private void DisableMovement()
    {
        movement.enabled = false;
    }

    private void EnableMovement()
    {
        movement.enabled = true;
    }

    //private void LookForTargets()
    //{
    //    Collider[] targets = Physics.OverlapSphere(transform.position, visionRange, targetMask);

    //    if (targets.Length > 0)
    //    {
    //        CollectTargets(targets); // collect any possible target
    //        movement.SetTarget(activeTarget);
    //    }
    //    else
    //        movement.SetTarget(null); // lost any possible target

    //    CheckTargets(targets);
    //}

    //private void CheckTargets(Collider[] possibleTargets)
    //{
    //    List<Transform> checkList = new List<Transform>();

    //    foreach (var t in possibleTargets)
    //    {
    //        checkList.Add(t.transform);
    //    }

    //    for (int i = 0; i < targets.Count; i++)
    //    {
    //        if (!checkList.Contains(targets[i]))
    //            targets.Remove(targets[i]);
    //    }
    //}

    //private void CollectTargets(Collider[] possibleTargets)
    //{
    //    for (int i = 0; i < possibleTargets.Length; i++)
    //    {
    //        if (!targets.Contains(possibleTargets[i].transform))
    //        {
    //            targets.Add(possibleTargets[i].transform);
    //        }
    //    }

    //    if (targets.Count == 1)
    //    {
    //        activeTargetIndex = 0;
    //        activeTarget = targets[0];
    //    }
    //}

    //private void SwitchTarget()
    //{
    //    if(targets.Count > 1)
    //    {
    //        activeTargetIndex++;

    //        if (activeTargetIndex >= targets.Count)
    //            activeTargetIndex = 0;

    //        activeTarget = targets[activeTargetIndex];
    //    }
    //}

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.cyan;
        //Gizmos.DrawWireSphere(transform.position, visionRange);

        //Gizmos.color = Color.red;

        //if(targets != null && targets.Count > 0)
        //    foreach (var target in targets)
        //    {
        //        Gizmos.DrawLine(transform.position + Vector3.up * 0.5f, target.position + Vector3.up * 0.5f);
        //    }
    }
}
