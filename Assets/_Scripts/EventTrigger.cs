﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTrigger : MonoBehaviour
{
    public bool oneTimeUse;

    public string triggerTag;

    public List<UnityEngine.Events.UnityEvent> onTriggerEnter, onTriggerStay, onTriggerExit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == triggerTag && onTriggerEnter != null)
            EventsListUtility.Invoke(onTriggerEnter);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == triggerTag && onTriggerStay != null)
            EventsListUtility.Invoke(onTriggerStay);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == triggerTag && onTriggerExit != null)
            EventsListUtility.Invoke(onTriggerExit);
    }
}