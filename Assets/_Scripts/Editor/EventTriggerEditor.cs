﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EventTrigger))]
public class EventTriggerEditor : Editor
{
    private EventTrigger trigger;

    private SerializedProperty oneTimeUse, triggerTag;

    private ReorderableList onTriggerEnter, onTriggerStay, onTriggerExit;

    private void OnEnable()
    {
        onTriggerEnter = new ReorderableList(serializedObject.FindProperty("onTriggerEnter"));
        onTriggerStay = new ReorderableList(serializedObject.FindProperty("onTriggerStay"));
        onTriggerExit = new ReorderableList(serializedObject.FindProperty("onTriggerExit"));

        oneTimeUse = serializedObject.FindProperty("oneTimeUse");
        triggerTag = serializedObject.FindProperty("triggerTag");
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(oneTimeUse);
        EditorGUILayout.PropertyField(triggerTag);
        EditorGUILayout.Space();

        onTriggerEnter.DoLayoutList();
        onTriggerStay.DoLayoutList();
        onTriggerExit.DoLayoutList();

        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
        }
    }
}