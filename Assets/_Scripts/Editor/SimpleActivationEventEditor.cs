﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SimpleActivationEvent))]
public class SimpleActivationEventEditor : Editor
{
    SimpleActivationEvent script;

    ReorderableList onEnableEvent;

    private void OnEnable()
    {
        onEnableEvent = new ReorderableList(serializedObject.FindProperty("onEnableEvent"));
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.Space();

        onEnableEvent.DoLayoutList();

        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();
    }
}