﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class EventsListUtility
{
    public static void Invoke(List<UnityEvent> events)
    {
        for (int i = 0; i < events.Count; i++)
        {
            events[i].Invoke();
        }
    }
}