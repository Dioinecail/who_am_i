﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHelper : MonoBehaviour
{
    public Camera targetCamera;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            targetCamera.orthographic = !targetCamera.orthographic;
    }
}