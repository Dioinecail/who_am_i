﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneManager : MonoBehaviour
{
    public static CutSceneManager Instance;

    private CutSceneController activeSceneBlock;
    private CutScene activeScene;

    private void Awake()
    {
        Instance = this;
    }

    public void PlayCutSceneBlock(CutSceneController block)
    {
        activeSceneBlock = block;
        activeSceneBlock.Play(0);
    }

    public void PlayCutScene(CutScene scene)
    {
        activeScene = scene;
        activeScene.Play();
    }

    public void PlayCutScene(CutScene scene, float time)
    {
        activeScene = scene;
        activeScene.Play(time);
    }

    public void PlayCutScene(CutScene scene, int frame)
    {
        activeScene = scene;
        activeScene.Play(frame);
    }

    public void SetActiveSceneBlock(CutSceneController block)
    {
        activeSceneBlock = block;
    }

    public void SetActiveScene(CutScene scene)
    {
        activeScene = scene;
    }

    public void StopCutScene()
    {
        if (activeScene != null)
            activeScene.Stop();
    }
}