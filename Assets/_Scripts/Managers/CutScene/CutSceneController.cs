﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneController : MonoBehaviour
{
    public List<CutScene> cutsceneBlocks;

    private CutScene activeScene;
    private int currentScene = 0;

    public void Play()
    {
        Play(currentScene);
    }

    public void Play(int index)
    {
        Play(index, 0f);
    }

    public void Play(int index, int frame)
    {
        Play(index, frame / 60f);
    }

    public void Play(int index, float time)
    {
        activeScene = cutsceneBlocks[index];
        CutSceneManager.Instance.SetActiveSceneBlock(this);
        activeScene.Play(time);
    }

    public void PlayNextScene()
    {
        activeScene.Stop();
        currentScene++;
        Play(currentScene, 0f);
    }

    public bool NextSceneAvailable()
    {
        return currentScene + 1 < cutsceneBlocks.Count;
    }

    public bool IsPlaying()
    {
        return activeScene != null && activeScene.IsPlaying();
    }
}