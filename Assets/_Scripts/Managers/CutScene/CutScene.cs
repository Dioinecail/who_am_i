﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CutScene : MonoBehaviour
{
    public PlayableDirector timeline;

    public void Play()
    {
        gameObject.SetActive(true);
        timeline.Play();
    }

    public void Play(float time)
    {
        SetTime(time);
        Play();
    }

    public void Play(int frame)
    {
        SetFrame(frame);
        Play();
    }

    public void Stop()
    {
        timeline.Stop();
        gameObject.SetActive(false);
    }

    public void SetFrame(int frame)
    {
        timeline.time = frame / 60d;
    }

    public void SetTime(float time)
    {
        timeline.time = time;
    }

    public bool IsPlaying()
    {
        return timeline.playableGraph.IsValid() && timeline.playableGraph.IsPlaying();
    }
}