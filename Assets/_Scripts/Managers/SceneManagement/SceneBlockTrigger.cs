﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneBlockTrigger : MonoBehaviour
{
    public SceneBlock block;
    public BlockAction action;

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            switch (action)
            {
                case BlockAction.Load:
                    SceneLoadManager.Instance.LoadBlock(block);
                    break;
                case BlockAction.Unload:
                    SceneLoadManager.Instance.UnloadBlock(block);
                    break;
                default:
                    break;
            }
        }
    }

#if UNITY_EDITOR

    BoxCollider triggerCollider;

    private void OnDrawGizmos()
    {
        if (triggerCollider == null)
            triggerCollider = GetComponent<BoxCollider>();

        switch (action)
        {
            case BlockAction.Load:
                Gizmos.color = Color.green;
                break;
            case BlockAction.Unload:
                Gizmos.color = Color.magenta;
                break;
            default:
                break;
        }

        Gizmos.DrawCube(transform.position, triggerCollider.size);
    }

#endif
}

public enum BlockAction
{
    Load,
    Unload
}