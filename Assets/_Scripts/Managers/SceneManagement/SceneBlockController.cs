﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneBlockController : MonoBehaviour
{
    public string blockId;
    public Transform sceneBlockRoot;

    private void Start()
    {
        SceneLoadManager.Instance.activeBlocks.Add(blockId, this);
    }

    public void SetBlockPosition()
    {
        if(SceneLoadManager.Instance != null && SceneLoadManager.Instance.loadedBlocks.ContainsKey(blockId))
        {
            SceneBlock block = SceneLoadManager.Instance.loadedBlocks[blockId];

            if (block != null)
                sceneBlockRoot.position = block.blockPosition;
        }
        else
        {
            Debug.LogError("Block " + blockId + " could not be found inside loadedBlocks dictionary");
        }
    }

    public void SetBlockPosition(SceneBlock block)
    {
        sceneBlockRoot.position = block.blockPosition;
    }

    private void OnDestroy()
    {
        SceneLoadManager.Instance.activeBlocks.Remove(blockId);
    }
}