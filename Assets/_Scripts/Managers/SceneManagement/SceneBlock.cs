﻿using UnityEngine;

[System.Serializable]
public class SceneBlock
{
    public string blockId;
    public Vector3 blockPosition;
}