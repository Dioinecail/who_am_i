﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadManager : MonoBehaviour
{
    public static SceneLoadManager Instance;

    public Dictionary<string, SceneBlock> loadedBlocks;
    public Dictionary<string, SceneBlockController> activeBlocks;

    private Queue<SceneBlock> loadingQueue;
    private Queue<SceneBlock> unloadingQueue;

    private void Start()
    {
        loadedBlocks = new Dictionary<string, SceneBlock>();
        loadingQueue = new Queue<SceneBlock>();
        unloadingQueue = new Queue<SceneBlock>();
        activeBlocks = new Dictionary<string, SceneBlockController>();

        Instance = this;
    }

    public void LoadBlock(SceneBlock block)
    {
        if(loadingQueue.Contains(block))
        {
            Debug.Log("Block " + block.blockId + " is already inside loading queue");
            return;
        }
        else
        {
            if (loadingQueue.Count > 0)
                loadingQueue.Enqueue(block);
            else
                StartCoroutine(StartLoadingBlock(block));
        }
    }

    public void UnloadBlock(SceneBlock block)
    {
        if (unloadingQueue.Contains(block))
        {
            Debug.Log("Block " + block.blockId + " is already inside UNloading queue");
            return;
        }
        else
        {
            if (unloadingQueue.Count > 0)
                unloadingQueue.Enqueue(block);
            else
                StartCoroutine(StartUnloadingBlock(block));
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Scene : " + scene + " has been loaded (" + mode.ToString() +")!");
    }

    private void OnSceneUnloaded(Scene scene)
    {
        Debug.Log("Scene : " + scene + " has been unloaded!");
    }

    private IEnumerator StartLoadingBlock(SceneBlock block)
    {
        if(loadedBlocks.ContainsKey(block.blockId))
        {
            Debug.Log("Block " + block.blockId + " has already been loaded!");
        }
        else
        {
            AsyncOperation loading = SceneManager.LoadSceneAsync(block.blockId, LoadSceneMode.Additive);

            while (!loading.isDone)
            {
                yield return null;
            }

            loadedBlocks.Add(block.blockId, block);

            if(activeBlocks.ContainsKey(block.blockId))
            {
                activeBlocks[block.blockId].SetBlockPosition(block);
            }

            Debug.Log("Block [" + block.blockId + "] loading has been finished!");
        }

        if (loadingQueue.Count > 0)
            yield return StartLoadingBlock(loadingQueue.Dequeue());
    }

    private IEnumerator StartUnloadingBlock(SceneBlock block)
    {
        if (loadedBlocks.ContainsKey(block.blockId))
        {
            AsyncOperation unloading = SceneManager.UnloadSceneAsync(block.blockId);

            while (!unloading.isDone)
            {
                yield return null;
            }

            loadedBlocks.Remove(block.blockId);

            Debug.Log("Block [" + block.blockId + "] unloading has been finished!");
        }
        else
        {
            Debug.Log("Block " + block.blockId + " has not been loaded!");
        }

        if (loadingQueue.Count > 0)
            yield return StartLoadingBlock(loadingQueue.Dequeue());
    }
}