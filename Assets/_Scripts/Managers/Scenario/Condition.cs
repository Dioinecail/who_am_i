﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName ="Condition_0", menuName = "Create Condition", order = 2)]
public class Condition : ScriptableObject
{
    public string task;

    public bool isDone;
}