﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName ="Scenario_Block_", menuName = "Create Scenario Block", order = 1)]
public class ScenarioBlock : ScriptableObject
{
    public List<Condition> conditions;
}