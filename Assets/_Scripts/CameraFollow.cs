﻿using System.Collections;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    private bool allowRotation = true;
    [Header("Transform referenses")]
    public Transform player;
    public Transform cameraXTransform;

    private Transform cameraObject;
    private Transform cameraRotationObject;
    private Transform cameraRotationLerpObject;

    private Vector3 cameraRotationObjectOffset;
    private Vector3 cameraOriginalOffset;
    private Vector3 currentCameraOffset;
    private Vector3 cameraOriginalPosition;
    private Rigidbody rb;

    public float leftRightMaxOffset;
    public float upDownMaxOffset;
    public float backMaxOffset;
    public float noiseThreshold;

    [Header("Camera Movement Speed")]
    public float cameraSpeed = 14f;
    public float cameraRotateSpeed = 10;
    public float cameraLerpSpeed = 0.75f;
    public float cameraLocalOffsetSpeed = 1;
    public float cameraVelocitySmoothing = 0.75f;
    public float cameraVerticalMaxAngle = 55f;
    public float cameraVerticalMinAngle = 55f;
    public float cameraOffsetLerpSpeed = 0.5f;

    [Header("JoyStick Compensation")]
    public float joystickCompensation = 2;

    [Header("Camera Wall Distance Compensation")]
    public float minimumDistance = 1;
    public float backRaycastDistance = 1;
    private float preWallDistance = 0;

    public float cameraRevertTime = 3;

    private float verticalRotation;
    private float verticalLerpRotation;
    private Vector3 deltaMousePosition = new Vector3(0,0);

    public LayerMask backwardMask;

    private Coroutine cameraNoiseCoroutine;

    private void Awake()
    {
        GameObject newCam = new GameObject("New Cam");
        newCam.hideFlags = HideFlags.HideAndDontSave;

        cameraRotationObject = GetComponent<Transform>();
        cameraObject = GetComponent<Transform>().GetChild(0).GetChild(0);

        cameraRotationLerpObject = newCam.transform;
        cameraRotationLerpObject.rotation = cameraRotationObject.rotation;

        cameraOriginalOffset = cameraObject.position - player.position;
        cameraOriginalPosition = cameraObject.localPosition;

        cameraRotationObjectOffset = cameraRotationObject.position - player.position;
        rb = player.GetComponent<Rigidbody>();
        verticalRotation = cameraXTransform.localEulerAngles.x;
    }

    private void Update()
    {
        //NoiseCamera(rb.velocity.magnitude);
        if (Input.GetKeyDown(KeyCode.LeftAlt))
            allowRotation = !allowRotation;

        deltaMousePosition.x = Mathf.Abs(Input.GetAxis("Right Stick X")) > 0 ? Input.GetAxis("Right Stick X") * joystickCompensation : allowRotation ? Input.GetAxis("Mouse X") : 0;
        deltaMousePosition.y = Mathf.Abs(Input.GetAxis("Right Stick Y")) > 0 ? Input.GetAxis("Right Stick Y") * joystickCompensation : allowRotation ? Input.GetAxis("Mouse Y") : 0;

        if (Input.GetMouseButtonDown(1))
            Cursor.lockState = Cursor.lockState == CursorLockMode.Locked ? CursorLockMode.None : CursorLockMode.Locked;
    }

    private void FixedUpdate()
    {
        PositionCamera();
        OffsetCamera();
    }

    private void PositionCamera()
    {
        // get player position
        Vector3 targetCameraObjectPosition = player.position;
        // add default offset to get exact camera object position
        targetCameraObjectPosition += cameraRotationObjectOffset;
        // add velocity to the position
        targetCameraObjectPosition += (rb != null ? rb.velocity * cameraVelocitySmoothing : Vector3.zero);
        // lerp camera position
        cameraRotationObject.position = Vector3.Lerp(cameraRotationObject.position, targetCameraObjectPosition, cameraSpeed * Time.fixedDeltaTime);
        // lerp camera object rotation
        cameraRotationLerpObject.Rotate(Vector3.up, deltaMousePosition.x * cameraRotateSpeed * Time.fixedDeltaTime);
        cameraRotationObject.rotation = Quaternion.Lerp(cameraRotationObject.rotation, cameraRotationLerpObject.rotation, cameraLerpSpeed * Time.fixedDeltaTime);
        // lerp vertical camera rotation
        verticalRotation += -deltaMousePosition.y * cameraRotateSpeed * Time.fixedDeltaTime;
        verticalRotation = Mathf.Clamp(verticalRotation, cameraVerticalMinAngle, cameraVerticalMaxAngle);

        verticalLerpRotation = Mathf.Lerp(verticalLerpRotation, verticalRotation, cameraLerpSpeed * Time.fixedDeltaTime);
        // actual lerp
        cameraXTransform.localEulerAngles = new Vector3(verticalLerpRotation, 0, 0);
    }

    private void OffsetCamera()
    {
        // get camera default local position
        Vector3 targetCameraLocalPosition = cameraOriginalPosition;

        float backWallDistance = RaycastBackward();

        if(backWallDistance > 0 && backWallDistance < 1.5f)
        {
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // TODO proper back raycasting offset
            targetCameraLocalPosition += -cameraObject.forward * backWallDistance;
        }

        cameraObject.localPosition = Vector3.Lerp(cameraObject.localPosition, targetCameraLocalPosition, cameraLocalOffsetSpeed * Time.deltaTime);
    }

    private float RaycastBackward()
    {
        RaycastHit backHit;

        if (Physics.Raycast(cameraObject.position, -cameraObject.forward, out backHit, backRaycastDistance, backwardMask))
        {
            return Vector3.Distance(cameraObject.position, backHit.point);
        }
        else
            return 0;
    }

    private void NoiseCamera(float currentVelocity)
    {
        if(currentVelocity > noiseThreshold)
            if (cameraNoiseCoroutine == null)
                cameraNoiseCoroutine = StartCoroutine(OffsetCameraCoroutine());
    }

    private IEnumerator OffsetCameraCoroutine()
    {
        currentCameraOffset = cameraOriginalOffset;

        float x = Random.Range(-leftRightMaxOffset, leftRightMaxOffset);
        float y = Random.Range(-upDownMaxOffset, upDownMaxOffset);
        float z = 0;

        while(rb.velocity.magnitude > noiseThreshold)
        {
            z = Mathf.Clamp(z - Time.deltaTime, -backMaxOffset, 0);
            currentCameraOffset = cameraOriginalOffset + new Vector3(x, y, z);

            cameraObject.localPosition = Vector3.Lerp(cameraObject.localPosition, currentCameraOffset, cameraOffsetLerpSpeed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }

        yield return RevertCamera();

        cameraNoiseCoroutine = null;
    }

    private IEnumerator RevertCamera()
    {
        currentCameraOffset = cameraOriginalOffset;

        float elapsedTime = 0;

        Vector3 oldPosition = cameraObject.localPosition;

        while (elapsedTime < cameraRevertTime && Vector3.Distance(cameraObject.localPosition, currentCameraOffset) > 0.01f)
        {
            elapsedTime += Time.deltaTime;
            cameraObject.localPosition = Vector3.Lerp(cameraObject.localPosition, currentCameraOffset, elapsedTime / cameraRevertTime);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnDrawGizmos()
    {
        if (cameraObject == null)
            cameraObject = GetComponent<Transform>().GetChild(0).GetChild(0);

        if(cameraRotationObject == null)
            cameraRotationObject = GetComponent<Transform>();

        if (cameraOriginalOffset == Vector3.zero)
            cameraOriginalOffset = cameraObject.position - player.position;

        if (cameraOriginalPosition == Vector3.zero)
            cameraOriginalPosition = cameraObject.localPosition;

        Gizmos.color = Color.white;
        Gizmos.DrawLine(player.position, cameraObject.position);

        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(cameraRotationObject.position + cameraRotationObject.rotation * cameraOriginalPosition, cameraObject.right * leftRightMaxOffset);
        Gizmos.DrawRay(cameraRotationObject.position + cameraRotationObject.rotation * cameraOriginalPosition, -cameraObject.right * leftRightMaxOffset);
        Gizmos.DrawRay(cameraRotationObject.position + cameraRotationObject.rotation * cameraOriginalPosition, cameraObject.up * upDownMaxOffset);
        Gizmos.DrawRay(cameraRotationObject.position + cameraRotationObject.rotation * cameraOriginalPosition, -cameraObject.up * upDownMaxOffset);
        Gizmos.DrawRay(cameraRotationObject.position + cameraRotationObject.rotation * cameraOriginalPosition, -cameraObject.forward * backMaxOffset);

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(cameraObject.position, cameraObject.position - cameraObject.forward * backRaycastDistance);
    }
}