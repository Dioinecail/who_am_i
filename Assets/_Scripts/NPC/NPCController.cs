﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    public List<string> dialogue;

    public List<AudioShot> dialogueAudio;
}