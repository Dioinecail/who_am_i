﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class AudioShot : MonoBehaviour, IComparable<AudioShot>
{
    public UnityEvent onShotAudioStart;
    public UnityEvent onShotAudioEnd;

    public AudioClip clip;
    public List<AudioEvent> audioEvents;

    public int CompareTo(AudioShot other)
    {
        int xName, yName;

        string firstNum = new string(name.Where(char.IsDigit).ToArray());
        string secondNum = new string(other.name.Where(char.IsDigit).ToArray());

        if (int.TryParse(firstNum, out xName))
        {
            if (int.TryParse(secondNum, out yName))
            {
                return xName - yName == 0 ? name.Length - other.name.Length : xName - yName;
            }
            return -1;
        }
        else
            return -1;
    }

#if UNITY_EDITOR
    public void StripAllComponents()
    {
        Component[] components = GetComponents<Component>();

        foreach (Component comp in components)
        {
            if (comp is Transform || comp is AudioShot)
                continue;
            else
                Undo.DestroyObjectImmediate(comp);
        }
    }
#endif
}