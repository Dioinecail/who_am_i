﻿using UnityEngine;

public class MoveBehaviour : AbstractBehaviour
{
    public float moveSpeed = 9f;
    public float runMultiplier = 1.45f;
    public float rotateSpeed = 4;
    public float accelerationSpeed = 17.5f;

    public float rollSpeed = 15f;

    private Transform target;
    private float targetVelocityMagnitude;
    private Vector3 currentVelocityVector;
    private Vector3 inputVelocity;
    private Vector3 horizontalVelocity;
    private Vector3 oldVelocity;

    private TiltController tiltController;
    private Transform cameraObject;

    private float angleBetweenVelocities;
    private bool canMove = true;

    private float xInput, yInput;

    protected override void Awake()
    {
        base.Awake();
        tiltController = GetComponent<TiltController>();
        cameraObject = FindObjectOfType<CameraFollow>().transform;
    }

    void Update()
    {
        if (canMove)
            ProcessInput();
        else
            SlowDown();
    }

    private void FixedUpdate()
    {
        Move();
        Rotate();
    }

    public void SetTarget(Transform t)
    {
        target = t;
    }

    private void ProcessInput()
    {
        xInput = Input.GetAxis("Horizontal");
        yInput = Input.GetAxis("Vertical");

        bool isRunning = Input.GetKey(KeyCode.LeftShift);

        Vector3 rightInput = cameraObject.right * xInput;
        Vector3 forwardInput = cameraObject.forward * yInput;

        inputVelocity = rightInput + forwardInput;

        inputVelocity = Vector3.ClampMagnitude(inputVelocity, 1) * moveSpeed * (isRunning ? runMultiplier : 1);

        if(!canMove)
            inputVelocity = Vector3.Lerp(inputVelocity, Vector3.zero, 0.25f);
    }

    private void Move()
    {
        oldVelocity = rBody.velocity;
        inputVelocity.y = rBody.velocity.y;

        rBody.velocity = Vector3.MoveTowards(rBody.velocity, inputVelocity, accelerationSpeed * Time.deltaTime);

        if (oldVelocity.magnitude < 0.1f && rBody.velocity.magnitude > 0.1f)
            tiltController.ForwardTilt();

        horizontalVelocity = rBody.velocity;
        horizontalVelocity.y = 0;

        Anima.SetFloat("Velocity", horizontalVelocity.magnitude);
    }

    private void SlowDown()
    {
        inputVelocity = Vector3.Lerp(inputVelocity, Vector3.zero, 0.25f);
    }

    public static float CalculateHorizontalAngle(Vector3 from, Vector3 to)
    {
        float direction = Mathf.Sign(Vector3.Cross(from, to).y);
        return Vector3.Angle(from, to) * direction;
    }

    private void Rotate()
    {
        Vector3 lookVector = rBody.velocity;
        lookVector.y = 0;

        Rotate(lookVector);
    }

    private void Rotate(Transform toTarget)
    {
        Vector3 lookDirection = toTarget.position - transform.position;
        lookDirection.y = 0;

        Rotate(lookDirection);
    }

    private void Rotate(Vector3 lookVector)
    {
        if (horizontalVelocity.magnitude < 0.1f)
        {
            lookVector = transform.forward;
        }

        Quaternion targetRotation = Quaternion.LookRotation(lookVector);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
    }

    public void OnAllowMovement()
    {
        canMove = true;
    }

    public void OnDisableMovement()
    {
        canMove = false;
    }

    private void OnEnable()
    {
        // TODO: Check movement block on finishing attack animation
        AttackBehaviour.onAttack += OnDisableMovement;
        AttackBehaviour.onFinishedAttack += OnAllowMovement;
        CollisionBehaviour.onLand += OnAllowMovement;
    }

    private void OnDisable()
    {
        AttackBehaviour.onAttack -= OnDisableMovement;
        AttackBehaviour.onFinishedAttack -= OnAllowMovement;
        CollisionBehaviour.onLand -= OnAllowMovement;
    }

    [Header("GUI")]
    public Vector2 guiBoxSize;

    public Vector2 guiBoxPosition;

    public float guiTextSpaces;


    private void OnGUI()
    {
        GUI.Box(new Rect(guiBoxPosition.x, guiBoxPosition.y + guiTextSpaces * 1, guiBoxSize.x, guiBoxSize.y), 
            "velocity.magnitude : " + rBody.velocity.magnitude.ToString("{0.00}"));

        GUI.Box(new Rect(guiBoxPosition.x, guiBoxPosition.y + guiTextSpaces * 2, guiBoxSize.x, guiBoxSize.y), 
            "angle : " + angleBetweenVelocities.ToString());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position, Vector3.up);

        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(transform.position, transform.forward);

        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(transform.position, inputVelocity);

        Gizmos.color = Color.red;
        if(rBody != null)
        Gizmos.DrawRay(transform.position, rBody.velocity);
    }
}