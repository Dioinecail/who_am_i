﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBehaviour : MonoBehaviour
{
    public static event OnAction onLand;
    public static event OnAction onJump;

    public Vector3 feetOffset;
    public float feetRadius;
    public LayerMask groundLayer;

    public bool onGround;
    public float secondsInMidAir
    {
        get; private set;
    }

    private void Update()
    {
        Collider[] ground = Physics.OverlapSphere(transform.position + feetOffset, feetRadius, groundLayer);

        if(!onGround && (ground != null && ground.Length > 0))
        {
            if (onLand != null)
                onLand.Invoke();
        }

        if(onGround && (ground == null || ground.Length == 0))
        {
            if (onJump != null)
                onJump.Invoke();
        }

        onGround = (ground != null && ground.Length > 0);

        if(onGround)
        {
            secondsInMidAir += Time.deltaTime;
        }
        else
        {
            secondsInMidAir = 0;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + feetOffset, feetRadius);
    }
}