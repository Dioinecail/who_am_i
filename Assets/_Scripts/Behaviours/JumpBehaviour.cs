﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : AbstractBehaviour
{
    public float jumpForce;
    public float jumpThrust = 1.25f;

    private bool canJump;

    private TiltController tiltController;

    protected override void Awake()
    {
        base.Awake();
        tiltController = GetComponent<TiltController>();
    }

    private void Update()
    {
        if(Input.GetButtonDown("Jump") && collisionBehaviour.onGround && canJump)
        {
            Jump();
        }
    }

    private void Jump()
    {
        tiltController.JumpForwardTilt();
        Vector3 newVelocity = rBody.velocity * jumpThrust;
        rBody.velocity = new Vector3(newVelocity.x, jumpForce, newVelocity.z);
        Anima.Play("Jump");
    }

    public void AllowJumping()
    {
        canJump = true;
    }

    public void DisableJumping()
    {
        canJump = false;    
    }

    private void OnEnable()
    {
        AttackBehaviour.onAttack += DisableJumping;
        AttackBehaviour.onFinishedAttack += AllowJumping;
        CollisionBehaviour.onLand += AllowJumping;
    }

    private void OnDisable()
    {
        AttackBehaviour.onAttack -= DisableJumping;
        AttackBehaviour.onFinishedAttack -= AllowJumping;
        CollisionBehaviour.onLand -= AllowJumping;
    }
}