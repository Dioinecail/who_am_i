﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnAction();

public abstract class AbstractBehaviour : MonoBehaviour
{
    protected Animator Anima;
    protected Rigidbody rBody;
    protected CollisionBehaviour collisionBehaviour;

    public AbstractBehaviour[] disableBehaviours;

    protected virtual void Awake()
    {
        Anima = GetComponent<Animator>();
        rBody = GetComponent<Rigidbody>();
        collisionBehaviour = GetComponent<CollisionBehaviour>();
    }

    public virtual void DisableBehaviours()
    {
        if(disableBehaviours != null)
            foreach (AbstractBehaviour behaviour in disableBehaviours)
            {
                behaviour.enabled = false;
            }
    }

    public virtual void EnableBehaviours()
    {
        if (disableBehaviours != null)
            foreach (AbstractBehaviour behaviour in disableBehaviours)
            {
                behaviour.enabled = true;
            }
    }
}
