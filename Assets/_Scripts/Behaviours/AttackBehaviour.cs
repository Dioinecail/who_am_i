﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AttackBehaviour : AbstractBehaviour
{
    public static event OnAction onAttack;
    public static event OnAction onFinishedAttack;

    private bool canAttackMidAir;
    private bool canAttackGround;

    public List<AttackAnimation> groundAnimations;
    public List<AttackAnimation> midAirAnimations;

    private AttackAnimation lastAttack;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if(canAttackGround)
            {
                GroundAttack();
            }
            else if (canAttackMidAir)
            {
                MidAirAttack();
            }
        }
    }

    public void AllowMidAirAttack()
    {
        canAttackMidAir = true;
        canAttackGround = false;
    }

    public void AllowGroundAttack()
    {
        canAttackGround = true;
        canAttackMidAir = false;
    }

    public void GroundAttack()
    {
        if (lastAttack != null && lastAttack.followUpAnimations != null && lastAttack.followUpAnimations.Count > 0)
            lastAttack = GetRandomAnimation(lastAttack.followUpAnimations);
        else
            lastAttack = GetRandomAnimation(groundAnimations);

        PlayAnimation(lastAttack.name);
        Anima.applyRootMotion = true;
        canAttackGround = false;
    }

    public void MidAirAttack()
    {
        PlayAnimation(GetRandomAnimation(midAirAnimations).name);
        canAttackMidAir = false;
    }

    public T GetRandomAnimation<T>(List<T> animations) where T : AttackAnimation
    {
        List<T> animationsList = new List<T>();
        animationsList.AddRange(animations.Where(x => !x.exclude).ToList());
        return animationsList[Random.Range(0, animationsList.Count)];
    }

    public void PlayAnimation(string animationName)
    {
        Anima.Play(animationName, 0, 0);
        if (onAttack != null)
            onAttack.Invoke();
    }

    public void AllowMovement()
    {
        if (onFinishedAttack != null)
            onFinishedAttack.Invoke();
    }

    public void OnFinishedGroundAttack()
    {
        canAttackGround = true;
        Anima.applyRootMotion = false;
        AllowGroundAttack();
        if (onFinishedAttack != null)
            onFinishedAttack.Invoke();
    }

    public void OnFinishedAirAttack()
    {
        canAttackMidAir = true;
        Anima.applyRootMotion = false;
        AllowMidAirAttack();
        if (onFinishedAttack != null)
            onFinishedAttack.Invoke();
    }

    private void ClearLastAttack()
    {
        lastAttack = null;
    }

    private void OnEnable()
    {
        // TODO: Setup new animations to have a good posing
        CollisionBehaviour.onLand += AllowGroundAttack;
        CollisionBehaviour.onJump += AllowMidAirAttack;
    }

    private void OnDisable()
    {
        CollisionBehaviour.onLand -= AllowGroundAttack;
        CollisionBehaviour.onJump -= AllowMidAirAttack;
    }
}

[System.Serializable]
public class AttackAnimation
{
    public bool exclude;
    public string name;
    public List<AttackAnimation> followUpAnimations;
}