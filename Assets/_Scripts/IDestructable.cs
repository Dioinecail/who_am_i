﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDestructable
{
    float health { get; set; }

    void TakeDamage(float dmg);
    void Die();
}
