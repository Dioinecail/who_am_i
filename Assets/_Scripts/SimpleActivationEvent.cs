﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleActivationEvent : MonoBehaviour
{
    public List<UnityEngine.Events.UnityEvent> onEnableEvent;

    void OnEnable()
    {
        EventsListUtility.Invoke(onEnableEvent);
    }
}