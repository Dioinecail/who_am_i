﻿    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class AudioController : MonoBehaviour
    {
        public bool playOnStart;

        public UnityEvent onAudioStartedPlaying;
        public UnityEvent onAudioFinishedPlaying;

        public List<AudioEvent> audioEvents;

        public AudioSource targetAudioSource;
        public AudioClip clip;
        public bool isLoop;
        public float delayTime;
        public float fadeInTime;
        public float volumeChangeDuration;
        public float VolumeChangeDuraction
        {
            get { return volumeChangeDuration; }
            set { volumeChangeDuration = value; }
        }

        private Coroutine onPlayCoroutine;
        public bool IsPlaying
        {
            get { return targetAudioSource.isPlaying; }
        }

        public bool ForceDisable { get; set; }

        private void Start()
        {
            if (playOnStart)
            {
                Play(delayTime);
            }
        }

        public void Play(AudioShot shot)
        {
            if (ForceDisable)
            {
                Debug.Log(string.Format("AudioController: {0} is marked as forceDisabled, but events from {1} are going to fire anyway", name, shot.name));
                if (shot.onShotAudioStart != null)
                    shot.onShotAudioStart.Invoke();

                if (shot.onShotAudioEnd != null)
                    shot.onShotAudioEnd.Invoke();

                return;
            }

            Debug.Log(string.Format("Playing audioShot : [{0}] - file : [{1}]", shot, shot.clip.name));
            clip = shot.clip;
            audioEvents = shot.audioEvents;
            onAudioStartedPlaying.AddListener(() =>
            {
                shot.onShotAudioStart.Invoke();
                onAudioStartedPlaying.RemoveAllListeners();
            });

            onAudioFinishedPlaying.AddListener(() =>
            {
                shot.onShotAudioEnd.Invoke();

                onAudioFinishedPlaying.RemoveAllListeners();
            });

            Play();
        }

        public void Play()
        {
            if (onPlayCoroutine != null)
            {
                StopCoroutine(onPlayCoroutine);
            }

            foreach (AudioEvent aEvent in audioEvents)
            {
                StartCoroutine(DelayedEventsTimer(aEvent.time, aEvent.onEvent));
            }

            if (targetAudioSource == null)
            {
                Debug.Log("AudioSource is null!");
                return;
            }

            if (clip == null)
            {
                Debug.Log("AudioClip is null!");
                return;
            }

            if (fadeInTime > 0)
                StartCoroutine(FadeInAudio(fadeInTime));

            targetAudioSource.loop = isLoop;
            targetAudioSource.clip = clip;

            targetAudioSource.Play();

            if (onAudioStartedPlaying != null)
            {
                onAudioStartedPlaying.Invoke();
                onAudioStartedPlaying.RemoveAllListeners();
            }

            onPlayCoroutine = StartCoroutine(WaitForAudioEnd(clip.length, onAudioFinishedPlaying));
        }


        public void Play(float delay)
        {
            Invoke("Play", delay);
        }

        public void Stop()
        {
            targetAudioSource.Stop();

            if (onPlayCoroutine != null)
                StopCoroutine(onPlayCoroutine);

            onPlayCoroutine = null;

            if (onAudioFinishedPlaying != null)
                onAudioFinishedPlaying.Invoke();
        }

        public void ChangeVolume(float newVolume)
        {
            StartCoroutine(ChangeAudioVolume(volumeChangeDuration, newVolume));
        }

        private IEnumerator DelayedEventsTimer(float timer, UnityEvent callback)
        {
            yield return new WaitForSeconds(timer);

            if (callback != null)
                callback.Invoke();
        }

        private IEnumerator ChangeAudioVolume(float duration, float newVolume)
        {
            float elapsedTime = 0;
            float cachedVolume = targetAudioSource.volume;

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;

                targetAudioSource.volume = Mathf.Lerp(cachedVolume, newVolume, elapsedTime / duration);

                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator FadeInAudio(float duration)
        {
            float elapsedTime = 0;
            float cachedVolume = targetAudioSource.volume;

            targetAudioSource.volume = 0;
            yield return new WaitForEndOfFrame();

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                targetAudioSource.volume = Mathf.Lerp(0, cachedVolume, elapsedTime / duration);
                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator WaitForAudioEnd(float seconds, UnityEvent callback)
        {
            yield return new WaitForSeconds(seconds);

            if (callback != null)
            {
                try
                {
                    callback.Invoke();
                    callback.RemoveAllListeners();
                }
                catch (Exception ex)
                {
                    Debug.Log("OMG HOW IS THIS POSSIBLE!!!??? : " + ex);
                }
            }

            onPlayCoroutine = null;
        }
    }
    [System.Serializable]
    public class AudioEvent
    {
        public float time;
        public UnityEvent onEvent;

        public AudioEvent(float t, UnityEvent e)
        {
            time = t;
            onEvent = e;
        }

        public void ExecuteEvent()
        {
            if (onEvent != null)
                onEvent.Invoke();
        }
    }