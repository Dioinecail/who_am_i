﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedUIShader : MonoBehaviour
{
    private Material uiMaterial;
    private Texture2D targetTexture;
    public float frequency;

    private int xCoord, yCoord;

    [Range(0.0001f, 0.01f)]
    public float lowestThreshold = 0.0005f;
    private bool allowedToSwitch;

    void Start()
    {
        uiMaterial = GetComponent<Image>().material;
        targetTexture = GetComponent<Image>().sprite.texture;
    }

    void Update()
    {
        AnimatePixelSelection();
    }

    private void AnimatePixelSelection()
    {
        if(uiMaterial.GetFloat("_ClosenessThreshold") < lowestThreshold && allowedToSwitch)
        {
            uiMaterial.SetColor ("_TargetColor", GetRandomPixel());
            allowedToSwitch = false;
        }
        else if (uiMaterial.GetFloat("_ClosenessThreshold") > lowestThreshold)
        {
            allowedToSwitch = true;
        }
    }

    public Color GetRandomPixel()
    {
        xCoord = Random.Range(0, targetTexture.width);
        yCoord = Random.Range(0, targetTexture.height);

        return targetTexture.GetPixel(xCoord, yCoord);
    }
}