Minimum Viable Prototype

AudioController +   
AudioShot +

SceneLoadManager +
SceneBlockController +
SceneBlock +

ScenarioManager / Linear +-
ScenarioBlock +-

CutSceneManager
CutSceneController +
CutScene +

EventTrigger +

NPCController  (contains scenario ID, audioshot etc.)

TODO: 
Work on Scenario blocks (chapters)
Work on Conditions (types, comparison)
Work on NPCs (audio, dialogue)